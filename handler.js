'use strict';

const queryString = require('query-string');
const _ = require('lodash');

const tasksManager = require('./tasksManager');
const slackPublisher = require('./slackPublisher');

function createResponse(statusCode, message) {
  return {
    statusCode: statusCode,
    body: JSON.stringify(message)
  };
}

module.exports.slackPublisher = (event, context, callback) => {
  console.log('slack publisher was called');
  slackPublisher();
  callback(null, null);
};

module.exports.tasks = (event, context, callback) => {
  const body = decodeURIComponent(event.body);
  const parsedBody = queryString.parse(body);
  //json body
  const command = parsedBody.command;
  const text = parsedBody.text;
  const userId = parsedBody.user_id;
  const username = parsedBody.user_name;

  if (command === '/tasks-new') {
    saveATask(text, userId, username, callback);
  } else if (command === '/task-get') {
    getATask(text, callback);
  } else if (command === '/task-pending') {
    getAllPendingTasks(callback);
  } else if (command === '/task-pending-user') {
    getAllPendingTasksForUser(userId, callback);
  } else if (command === '/task-complete') {
    completeATask(text, callback);
  } else {
    callback(null, createResponse(200, 'Another method was called'));
  }
};

function saveATask(text, userId, username, callback) {
  // Format text: task Title,2017-10-20 12:00,task Description
  const textArray = text.split(',');
  const taskTitle = textArray[0];
  const dueDate = textArray[1];
  // But the description can have commas!
  const taskDescription = _.tail(_.tail(textArray)).join();

  console.log(taskTitle);
  console.log(dueDate);
  console.log(taskDescription);
  console.log(userId + ' ' + username);

  tasksManager
    .saveTask(taskTitle, taskDescription, dueDate, userId, username)
    .then(result => {
      console.log(result);
      callback(null, createResponse(200, result));
    })
    .catch(error => {
      console.log(error);
      callback(null, createResponse(500, 'Error saving a task'));
    });
}

function getATask(text, callback) {
  console.log(text);

  tasksManager
    .findTaskById(text)
    .then(result => {
      console.log(result);
      callback(null, createResponse(200, result));
    })
    .catch(error => {
      console.log(error);
      callback(null, createResponse(500, 'Error getting a task'));
    });
}

function getAllPendingTasks(callback) {
  tasksManager
    .getExpiringTasksIn24Hrs()
    .then(result => {
      console.log(result);
      callback(null, createResponse(200, { attachments: result }));
    })
    .catch(error => {
      console.log(error);
      callback(null, createResponse(500, 'Error getting all pending tasks'));
    });
}

function getAllPendingTasksForUser(userId, callback) {
  tasksManager
    .getPendingTasksForUser(userId)
    .then(result => {
      console.log(result);
      callback(null, createResponse(200, { attachments: result }));
    })
    .catch(error => {
      console.log(error);
      callback(null, createResponse(500, 'Error getting all pending tasks for user'));
    });
}

function completeATask(text, callback) {
  const taskId = text;

  tasksManager
    .deleteTask(taskId)
    .then(result => {
      console.log(result);
      callback(null, createResponse(200, { text: `Task ${taskId} was completed` }));
    })
    .catch(error => {
      console.log(error);
      callback(null, createResponse(500, 'Error getting all pending tasks for user'));
    });
}
