'use strict';

const _ = require('lodash');
const dateFns = require('date-fns');

module.exports.formatAttachment = tasksList => {
  const attachments = _.map(tasksList, task => {
    const attachment = {};
    attachment.fallback = task.taskDescription;
    attachment.color = '#333333';
    attachment.title = task.taskTitle;
    attachment.text = task.taskDescription;
    attachment.fields = [];
    var field1 = {};
    field1.title = 'Task Id';
    field1.value = task.taskId;
    attachment.fields.push(field1);

    var field2 = {};
    field2.title = 'Due date';
    field2.value = dateFns.format(dateFns.parse(task.dueDate), 'YYYY-MM-DDTHH:mm');
    attachment.fields.push(field2);

    console.log(attachment);
    return attachment;
  });

  console.log(attachments);
  return attachments;
};

/*
{
    "attachments": [
        {
            "color": "#333333",
            "author_name": "Username",
            "title": "Task title",
            "text": "Task description",
            "fields": [
                {
                    "title": "Task id",
                    "value": "TaskId"
                },
				{
                    "title": "Due date",
                    "value": "Duedate"
                }
            ],
            "footer": "Learning serverless tasks manager",
            "ts": 123456789
        }
    ]
}
*/
