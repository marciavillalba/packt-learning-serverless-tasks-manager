'use strict';

const uuidv1 = require('uuid/v1');
const AWS = require('aws-sdk');
const dynamo = new AWS.DynamoDB.DocumentClient();
const dateFns = require('date-fns');

const tasksFormatter = require('./tasksFormatter');

module.exports.saveTask = (taskTitle, taskDescription, dueDate, userId, username) => {
  const task = {};
  task.taskId = uuidv1();
  task.taskTitle = taskTitle;
  task.taskDescription = taskDescription;
  task.dueDate = dateFns.getTime(dateFns.parse(dueDate));
  task.userId = userId;
  task.username = username;

  const params = {
    TableName: process.env.TASKS_DYNAMODB_TABLE,
    Item: task
  };

  return dynamo.put(params).promise().then(() => {
    return task.taskId;
  });
};

module.exports.deleteTask = taskId => {
  const params = {
    Key: {
      taskId: taskId
    },
    TableName: process.env.TASKS_DYNAMODB_TABLE
  };

  return dynamo.delete(params).promise();
};

module.exports.findTaskById = taskId => {
  const params = {
    Key: {
      taskId: taskId
    },
    TableName: process.env.TASKS_DYNAMODB_TABLE
  };

  return dynamo.get(params).promise();
};

module.exports.getExpiringTasksIn24Hrs = () => {
  const today = Number(dateFns.getTime(new Date())); //now in number and milisecs
  const tomorrow = Number(dateFns.getTime(dateFns.addSeconds(today, 86400))); //24 hrs from now 24*60*60

  console.log(today);
  console.log(tomorrow);

  const params = {
    ExpressionAttributeNames: {
      '#dd': 'dueDate'
    },
    ExpressionAttributeValues: {
      ':tomorrow': tomorrow,
      ':today': today
    },
    FilterExpression: '#dd between :today and :tomorrow',
    TableName: process.env.TASKS_DYNAMODB_TABLE
  };

  return dynamo.scan(params).promise().then(tasksList => {
    return tasksFormatter.formatAttachment(tasksList.Items);
  });
};

module.exports.getPendingTasksForUser = userId => {
  const params = {
    ExpressionAttributeNames: {
      '#userId': 'userId'
    },
    ExpressionAttributeValues: {
      ':userId': userId
    },
    FilterExpression: '#userId = :userId',
    TableName: process.env.TASKS_DYNAMODB_TABLE
  };

  return dynamo.scan(params).promise().then(tasksList => {
    return tasksFormatter.formatAttachment(tasksList.Items);
  });
};
