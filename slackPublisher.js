'use strict';

const rp = require('request-promise');
const tasksManager = require('./tasksManager');

const SLACK_URL = '<your incoming webhook url>';

module.exports = () => {
  tasksManager.getExpiringTasksIn24Hrs().then(result => {
    return sendMessageToSlack({ attachments: result });
  });
};

function sendMessageToSlack(text) {
  const options = {
    method: 'POST',
    uri: SLACK_URL,
    json: true,
    body: text
  };

  console.log(options);

  return rp(options);
}
